<?php 
    include 'inc/controller.php';
 ?>

<!doctype html>
<html lang='pt-BR'>
<head>

    <meta charset='UTF-8'>

    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>

    <title>Corporate Park</title>

    <!-- Favicon -->
    <link rel='icon' href='assets/img/favicon.ico'>

    <!-- Css -->
    <link rel='stylesheet' href='assets/css/main.css'>
    <link rel='stylesheet' href='assets/css/fonts.css'>

    <!-- Analytics -->
    <!-- <script src='assets/js/analytics.js'></script> -->

    <!-- HTML5 Shiv -->
    <!--[if lt IE 9]><script src='assets/js/html5.js'></script><![endif]-->

</head>
<body>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-73201231-1', 'auto');
	ga('send', 'pageview');
	</script>

    <?php require 'pages/'.$page.'.php'; ?>

    <!-- Js -->
    <script src='assets/js/jquery.js'></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script src='assets/js/input-validate.js'></script>
    <script src='assets/js/send-form.js'></script>
    <script src='assets/js/scroll.js'></script>
    <script src='assets/js/modal.js'></script>
    <script src='assets/js/map.js'></script>
    <script src='assets/js/slider.js'></script>
    <script src='assets/js/swipe.js'></script>


</body>
</html>