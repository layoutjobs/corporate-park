
<header>
  <div class="wrap">
    <h1> <span class="big">Galpões</span><span class="small">Modulares</span></h1>
    <button><span class="stripe"></span><span class="stripe"></span><span class="stripe"></span></button>
    <nav><a data-section="#company">Galpões</a><a data-section="#map" class="middle">Localização</a><a data-section="#contact">Vendas</a></nav>
  </div>
</header>
<div id="banner">
  <div class="wrap">
    <div class="text">
      <h2>LOTEAMENTO INDUSTRIAL</h2><span>
        <p>PÉ DIREITO COM 10 METROS </p>
        <p>GALPÕES DE 1.330 A 2.320m2</p>
        <p>EXCELENTE LOCALIZAÇÃO</p></span>
    </div>
  </div>
</div>
<div id="company">
  <div class="wrap">
    <h2>Instale sua empresa aqui!</h2>
    <h3>GALPÕES INDUSTRIAIS NO CORPORATE PARK</h3>
    <div class="row">
      <div class="col left">
        <figure data-gallery="#hangar" class="hangar thumb"><img src="assets/img/icon-01.png" class="icon"/>
          <figcaption>Clique para visualizar as imagens ilustrativas</figcaption>
        </figure>
        <div class="text">
          <h2>LOCALIZAÇÃO<span>GALPÕES</span></h2>
          <p>Os galpões modulares serão edificados no loteamento industrial Corporate Park para atender empresas de pequeno e médio porte. O loteamento tem topografia diferenciada, oferece portaria blindada, controle de acesso, ruas projetadas para tráfego de caminhões e centro administrativo com sala de reunião, mini auditório e ambulatório.</p>
        </div>
      </div>
      <div class="spacer"></div>
      <div class="col right">
        <figure data-gallery="#corporate" class="corporate thumb"><img src="assets/img/icon-02.png" class="icon"/>
          <figcaption>Clique para visualizar as imagens ilustrativas</figcaption>
        </figure>
        <div class="text">
          <h2>EXCELENTE <span>LOCALIZAÇÃO</span></h2>
          <p>O loteamento industrial Corporate Park está localizado em uma região privilegiada próximo aos principais centros urbanos e industriais do estado de São Paulo. As margens da SP-75, o empreendimento oferece rápido acesso a Sorocaba, Indaiatuba, Campinas, Jundiaí e São Paulo, além do fácil acesso as principais rodovias como Bandeirantes, Anhanguera e Castelo Branco. Por tudo isso, os galpões modulares são um excelente investimento em logística para a sua empresa.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col left">
        <figure data-number="3" class="table_thumb"><img src="assets/img/icon-03.png" class="icon icon-03"/>
          <figcaption>RELAÇÃO DE DISTÂNCIAS CIDADES, RODOVIAS, ETC</figcaption>
        </figure>
      </div>
      <div class="col right">
        <div class="text">
          <h2>GALPÕES<span>MODULADOS</span></h2>
          <p>Através da parceria da Salto Empreendimentos Imobiliários com a empresa Pré Moldados Paraná, serão construídos galpões industriais de 1330 m² até 2320 m² com pé direito de 10 metros. Todos os galpões serão edificados respeitando as normas de segurança e acessibilidade exigidas, atendendo tanto empresas industriais como as de logística, com mezaninos amplos para escritório, acesso e piso para entrada de caminhões. A altura de 10 metros livre é um grande diferencial. Outro diferencial é que, pelos galpões serem vendidos na planta, poderemos alterar o projeto de acordo com a necessidade do cliente, como por exemplo, alterações da altura, do tamanho do escritório, utilização de docas, utilização de pontes rolantes e cores do fechamento.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="map"></div>
<div id="contact">
  <div class="wrap">
    <h2 class="bold">VENDAS</h2><a>(15) 9 7403.9369</a><a>(11) 9 9179.3805</a>
    <form data-form="#contact_form" onsubmit="send_form(this); return false" id="contact_form">
    <!-- <form data-form="#contact_form" method="post" action="inc/send-contact.php" id="contact_form"> -->
      <div class="textbox">
        <input type="text" id="name" name="name" required="required" class="text name"/>
        <label for="name">Nome</label>
      </div>
      <div class="textbox">
        <input type="email" id="email" name="email" data-form="#contact_form" required="required" class="text email"/>
        <label for="email">Email</label>
      </div>
      <div class="textbox">
        <input type="tel" id="phone" name="phone" class="text phone"/>
        <label for="phone">Telefone</label>
      </div>
      <div class="textbox">
        <textarea id="msg" name="msg" class="text msg"></textarea>
        <label for="msg">Mensagem</label>
      </div>
      <button class="submit">enviar</button>
    </form>
  </div>
</div>
<footer>
  <div class="wrap">
    <figure>
      <figcaption>REALIZAÇÃO</figcaption><img src="assets/img/salto.png" alt=""/>
    </figure>
    <figure>
      <figcaption>OBRAS</figcaption><img src="assets/img/parana.png" alt=""/>
    </figure>
  </div>
</footer>
<div class="modal background"></div>
<div class="modal banner">
  <div class="close-button">&times;</div>
  <div id="hangar" class="gallery"><img data-number="0" src="assets/img/hangar-01.jpg" alt="" class="active"/><img data-number="1" src="assets/img/hangar-02.jpg" alt=""/><img data-number="2" src="assets/img/hangar-03.jpg" alt=""/></div>
  <div id="corporate" class="gallery"><img data-number="0" src="assets/img/corporate-park-01.jpg" alt="" class="active"/><img data-number="1" src="assets/img/corporate-park-02.jpg" alt=""/><img data-number="2" src="assets/img/corporate-park-03.jpg" alt=""/><img data-number="3" src="assets/img/corporate-park-04.jpg" alt=""/><img data-number="4" src="assets/img/corporate-park-05.jpg" alt=""/><img data-number="5" src="assets/img/corporate-park-06.jpg" alt=""/></div><span class="counter"></span>
  <button title="Clique para ver a imagem anterior" class="ctrl left"></button>
  <button title="Clique para ver a próxima imagem" class="ctrl right"></button>
</div>
<div data-displayed="false" class="modal form">
  <div class="close-button">&times;</div>
  <div class="title"> 
    <p class="bold">A Deltalog liga pra você</p>
    <p>Preencha as informações abaixo e nosso consultor entrará em contato</p>
  </div>
  <div class="shake_title">
    <p class="bold">Só um minuto</p>
    <p>Gostaria de receber mais informações sem compromisso? Preencha as informações abaixo e nosso consultor entrará em contato</p>
  </div>
  <form data-form="#modal_form" onsubmit="send_form(this); return false" id="modal_form">
    <div>
      <input type="text" id="name2" name="name" required="required" class="text name"/>
      <label for="name2">Nome</label>
    </div>
    <div>
      <input type="email" id="email2" name="email" data-form="#modal_form" required="required" class="text email"/>
      <label for="email2">Email</label>
    </div>
    <div>
      <input type="tel" id="phone2" name="phone" required="required" class="text phone"/>
      <label for="phone2">Telefone</label>
    </div>
    <div>
      <textarea id="msg2" name="msg" required="required" class="text msg"></textarea>
      <label for="msg2">Mensagem</label>
    </div>
    <div>
      <button class="submit">enviar</button>
    </div>
  </form>
</div>
<div class="modal table">
  <ul class="cities">
    <li class="title">DISTÂNCIAS DE CIDADES</li>
    <li><span class="text">Indaiatuba</span><span class="value">19km</span></li>
    <li><span class="text">Campinas</span><span class="value">47km</span></li>
    <li><span class="text">Sorocaba</span><span class="value">44km</span></li>
    <li><span class="text">Jundiaí</span><span class="value">60km</span></li>
    <li><span class="text">Piracicaba</span><span class="value">70km</span></li>
    <li><span class="text">São Paulo</span><span class="value">123km</span></li>
    <li><span class="text">Santos</span><span class="value">191km</span></li>
  </ul>
  <ul class="highways">
    <li class="title">DISTÂNCIA DE RODOVIAS</li>
    <li> <span class="text">Rod. dos Bandeirantes (Campinas)</span><span class="value">35km</span></li>
    <li> <span class="text">Rod. Castelo Branco (Sorocaba)</span><span class="value">26km</span></li>
    <li> <span class="text">Rod. Anhanguera (Campinas)</span><span class="value">45km</span></li>
    <li> <span class="text">Rodoanel</span><span class="value">86km</span></li>
    <li> <span class="text">Marginal Tietê</span><span class="value">100km</span></li>
    <li> <span class="text">Marginal Pinheiros</span><span class="value">102km</span></li>
  </ul>
  <ul class="airports">
    <li class="title"> DISTÂNCIA DE PORTOS E AEROPORTOS</li>
    <li><span class="text">Viracopos (Campinas)</span><span class="value">27km</span></li>
    <li><span class="text">Congonhas (São Paulo)</span><span class="value">109km</span></li>
    <li><span class="text">Guarulhos</span><span class="value">126km</span></li>
    <li><span class="text">Porto de Santos</span><span class="value">192Km</span></li>
  </ul>
</div>