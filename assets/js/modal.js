thumb              = document.querySelectorAll('#company .row .col .thumb');
modal_background   = document.querySelector('.modal.background');
modal_close_button = document.querySelector('.modal.banner .close-button');
modal_banner       = document.querySelector('.modal.banner');
gallery            = document.querySelectorAll('.gallery');
table              = document.querySelector('.modal.table');

modal_banner_left_arrow  = document.querySelector('.modal.banner .ctrl.left');
modal_banner_right_arrow = document.querySelector('.modal.banner .ctrl.right');


document.querySelector('.table_thumb').onclick = function() {

    modal_background.style.display = 'block';
    table.classList.add('active');

}




function show_banner(element) {
    
    counter = document.querySelector('.modal.banner .counter');

    gallery = element.getAttribute('data-gallery');
    gallery = document.querySelector(gallery);

    gallery.classList.add('active');
    
    modal_banner.style.display = 'block';
    modal_background.style.display = 'block';

    n = parseInt( document.querySelector('.gallery.active img.active').getAttribute('data-number') ) +1;
    images = document.querySelectorAll('.modal.banner .gallery.active img');

    counter.innerHTML = 'Imagem ' + n + ' de ' + images.length;

}



function close_modal() {

    modal = document.querySelectorAll('.modal');
    gallery = document.querySelectorAll('.gallery');

    for ( var i = 0 ; i < modal.length ; i++ ) {
        modal[i].style.opacity = 0;
    }

    setTimeout(function() {

        for ( var i = 0 ; i < modal.length ; i++ ) {
            modal[i].removeAttribute('style');
            modal[i].classList.remove('active');
        }

        for ( var i = 0 ; i < gallery.length ; i++ ) {
            gallery[i].classList.remove('active');
        }

    },1000);

}



function previous_banner() {

    images = document.querySelectorAll('.modal.banner .gallery.active img');
    counter = document.querySelector('.modal.banner .counter');
    n      = document.querySelector('.modal.banner .gallery.active img.active').getAttribute('data-number');

    n <= 0 ? n = (images.length - 1) : n--;

    for ( var i = 0 ; i < images.length ; i++ ) {
        images[i].classList.remove('active');
    }

    images[n].classList.add('active');
    counter.innerHTML = 'Imagem ' + (n+1) + ' de ' + images.length;

}



function next_banner() {

    images = document.querySelectorAll('.modal.banner .gallery.active img');
    counter = document.querySelector('.modal.banner .counter');
    n      = document.querySelector('.modal.banner .gallery.active img.active').getAttribute('data-number');
    
    n >= (images.length - 1) ? n = 0 : n++;

    for ( var i = 0 ; i < images.length ; i++ ) {
        images[i].classList.remove('active');
    }

    images[n].classList.add('active');
    counter.innerHTML = 'Imagem ' + (n+1) + ' de ' + images.length;
}

table.onclick = close_modal;
modal_background.onclick = close_modal;
modal_close_button.onclick = close_modal;

modal_banner_left_arrow.onclick = previous_banner;
modal_banner_right_arrow.onclick = next_banner;


for ( var i = 0 ; i < thumb.length ; i++ ) {
    
    thumb[i].onclick = function() {
        show_banner(this);
    }

}