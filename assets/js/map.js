var map_center = new google.maps.LatLng(-23.2145706,-47.318618);
var map_icon_position = new google.maps.LatLng(-23.2145706,-47.318618);




function initialize() {
  var isDraggable = $(document).width() > 480 ? true : false;

   var mapOptions = {
       center: map_center,
       disableDefaultUI: true,
       zoomControl: true,
       panControl: true,
       streetViewControl: true,
       scrollwheel: false,
       draggable: isDraggable,
       zoom: 16,
       navigationControl: false,
       mapTypeControl: false,
       scaleControl: false,
       mapTypeId: google.maps.MapTypeId.SATELLITE
   };

   var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  
   // opções do marcador
   var marker = new google.maps.Marker({
      position: map_icon_position,
      map: map,
      title:"Greenpark",
      icon: 'assets/img/map-icon.png',
      animation: google.maps.Animation.DROP
  });

}

google.maps.event.addDomListener(window, 'load', initialize);