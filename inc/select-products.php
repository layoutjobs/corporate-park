<?php 
	require 'connect.php';

	extract($_GET);

	// $sql = 'select * from tbl_products where category = "'.$category.'"';
	$sql = 'select * from tbl_products';
	$query = mysqli_query($con, $sql)or die($sql);
	while ( $rs = mysqli_fetch_array($query) ) {
		
		extract($rs);

		echo '
			<div class="row">
				<div class="wrap">
					<figure  class="icon">
						<img src="assets/img/magnify-icon.png" alt="" class="v-centered">
					</figure>

					<figure class="photo">
						<img src="assets/img/'.$img.'" alt="" class="v-centered">
					</figure>
					
					<span class="title">
						<h3 class="bold">'.$name.'</h3>

						<a href="'.$pdf.'">
							<img src="assets/img/pdf-icon.png" alt="">
						</a>
					</span>

					<span class="characteristics">
						
						<p class="item">
							<span class="bold">Potência:</span>
							<span class="text">'.$output.'</span>
						</p>
						
						<p class="item">
							<span class="bold">Vida útil do LED:</span>
							<span class="text">'.$lifetime.'</span>
						</p>
						
						<p class="item">
							<span class="bold">Voltagem:</span>
							<span class="text">'.$voltage.'</span>
						</p>
						
						<p class="item">
							<span class="bold">Temperatura de Cor:</span>
							<span class="text">'.$color_temperature.'</span>
						</p>
						
						<p class="item">
							<span class="bold">Ângulo de Luz:</span>
							<span class="text">'.$light_angle.'</span>
						</p>
						
						<p class="item">
							<span class="bold">Lâmpada:</span>
							<span class="text">'.$lamp.'</span>
						</p>
						
						<p class="item">
							<span class="bold">Material:</span>
							<span class="text">'.$material.'</span>
						</p>

						<p class="item">
							<span class="bold">IP:</span>
							<span class="text">'.$ip.'</span>
						</p>
					</span>
				</div>
			</div>
		';
	}





?>

