<?php
    $page = !empty($_GET['page']) ? $_GET['page'] : 'home';
    
    if ($page == 'home') { 
        
        $links  =   '<ul class="menu">
                        <li><a data-section="#home">HOME</a></li>
                        <li><a>PRODUTOS</a>
                            <ul class="submenu">
                                <li><a href="produtos/publica"> ILUMINAÇÃO PÚBLICA</a></li>
                                <li><a href="produtos/industrial"> ILUMINAÇÃO INDUSTRIAL</a></li>
                                <li><a href="produtos/comercial"> ILUMINAÇÃO COMERCIAL</a></li>
                                <li><a href="produtos/residencial"> ILUMINAÇÃO RESIDENCIAL</a></li>
                            </ul>
                        </li>
                        <li><a data-section="#applications">APLICAÇÕES</a></li>
                        <li><a data-section="#company">EMPRESA</a></li>
                        <li><a data-section="#buy">COMPRAR</a></li>
                        <li><a data-section="#contact">CONTATO</a></li>
                    </ul>';

    } else {
        $links  =   '<ul class="menu">
                        <li><a href="./">HOME</a></li>
                        <li><a href="produtos">PRODUTOS</a>
                            <ul class="submenu">
                                <li><a href="produtos/publica"> ILUMINAÇÃO PÚBLICA</a></li>
                                <li><a href="produtos/industrial"> ILUMINAÇÃO INDUSTRIAL</a></li>
                                <li><a href="produtos/comercial"> ILUMINAÇÃO COMERCIAL</a></li>
                                <li><a href="produtos/residencial"> ILUMINAÇÃO RESIDENCIAL</a></li>
                            </ul>
                        </li>
                        <li><a href="./#applications">APLICAÇÕES</a></li>
                        <li><a href="empresa">EMPRESA</a></li>
                        <li><a href="./#buy">COMPRAR</a></li>
                        <li><a href="contato">CONTATO</a></li>
                    </ul>';

    }


    // $title_ptn = 'Condomínio Deltalog';
    $title = 'Proval';

    // switch($page) {

    //     case 'home':
    //         $title = 'Home | '.$title_ptn;
    //         break;

    //     default:
    //         $title = $title_ptn;

    // }

?>